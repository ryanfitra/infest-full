<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['namespace' => 'Api', 'middleware' => ['auth:sanctum']], function () {

    Route::group(['prefix' => 'acl', 'namespace' => 'Acl'], function () {
        Route::apiResource('user-data', 'UserApiController', ['only' => ['index']]);
    });

    Route::resource('books', 'BooksApiController');
    Route::get('books-filter', 'BooksApiController@filter');
    Route::resource('readers', 'ReadersApiController');
});
