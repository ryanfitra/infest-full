<?php

namespace App\Http\Controllers\Api;

use App\Http\Resource\JsonCollerctionResource;
use App\Models\Readers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ReadersApiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $limit = $request->has('limit') ? $request->get('limit') : 10;

        return new JsonCollerctionResource(Readers::with(['books'])->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' =>'required',
            'phone' =>'required',
            'address'   =>'required',
            'book_id' =>'required',
        ]);

        $reader = Readers::create($request->all());

        return (new JsonCollerctionResource($reader))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(Readers $readers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Readers $readers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Readers $readers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Readers $readers)
    {
        //
    }
}
