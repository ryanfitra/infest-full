<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Readers extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'phone',
        'address',
        'book_id'
    ];

    public function books()
    {
        return $this->belongsTo(Books::class, 'book_id', 'id');
    }
}
