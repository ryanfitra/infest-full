import Sidebar from "./SideBar.vue"
import SideBarLink from "./SideBarLink.vue";

const SidebarPlugin = {
    install(Vue, options) {
        Vue.component('sidebar', Sidebar);
        Vue.component('sidebar-link', SideBarLink);
    }
}

export default SidebarPlugin;
