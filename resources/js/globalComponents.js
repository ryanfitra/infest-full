import { BootstrapVue, BootstrapVueIcons } from "bootstrap-vue"
import fontAwesome from './fontAwesome'
import SideBar from "@views/layout/sidebar";

// Import Bootstrap and BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

const GlobalComponents = {
    install(Vue) {
        Vue.use(BootstrapVue, BootstrapVueIcons)
        Vue.use(fontAwesome)
        Vue.use(SideBar)
    }
}

export default GlobalComponents
